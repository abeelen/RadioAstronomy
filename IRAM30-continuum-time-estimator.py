#!/usr/bin/env python

__version__ =  '2014.09.13'

import sys, argparse
from datetime import timedelta

import numpy as np

class objectview(object):
    def __init__(self, d):
        self.__dict__ = d


def select_instrument(name):
    # Define the instrument properties

    GISMO = { 'Np':100.           ,          #Number of pixels
              'Sp':13.75          ,          #Pixel size (in arcsec)
              'HPBW':16.7         ,          #Half-power beamwidth (in arcsec)
              'Wavelength':2000.  ,          #Central wavelength in microns
              'NEFD':14.                     #Noise ==uivalent flux density
          }

    NIKA1mm = { 'Np':136.           ,        #Number of pixels
                'Sp':6.8            ,        #Pixel size (in arcsec)
                'HPBW':12.          ,        #Half-power beamwidth (in arcsec)
                'Wavelength':1250.  ,        #Central wavelength in microns
                'NEFD':35.                   #Noise equivalent flux density
            }

    NIKA2mm = { 'Np':114.           ,        #Number of pixels
                'Sp':9.6            ,        #Pixel size (in arcsec)
                'HPBW':17.5         ,        #Half-power beamwidth (in arcsec)
                'Wavelength':2000.  ,        #Central wavelength in microns
                'NEFD':14.                   #Noise equivalent flux density
            }

    XPoint = False
    YPoint = False

    if args.camera == 'GISMO':
        instr=objectview(GISMO)
        if args.Xsize <= 1.:
            args.Xsize = 1.8
            XPoint = True
        if args.Ysize <= 1.8:
            args.Ysize = 3.7
            YPoint = True
    elif args.camera == 'NIKA1mm':
        instr=objectview(NIKA1mm)
        if args.Xsize <= 1.:
            args.Xsize = 1.8
            XPoint = True
        if args.Ysize <= 1.:
            args.Ysize = 1.8
            YPoint = True
    elif args.camera == 'NIKA2mm':
        instr = objectview(NIKA2mm)
        if args.Xsize <= 1.0:
            args.Xsize  = 2.0
            XPoint = True
        if args.Ysize <= 1.0:
            args.Ysize  = 2.0
            YPoint = True

    if instr.Wavelength == 1250.:
        tau = {2: 0.22, 4: 0.36, 7: 0.57}
    elif instr.Wavelength == 2000.:
        tau = {2: 0.06, 4: 0.11, 7: 0.19}

    if XPoint and YPoint:
        args.Point = True

    return instr, tau

def time_estimator(args,instr):
    transmission = np.exp(-tau[args.pwv]/np.sin(args.elevation*np.pi/180.))
    # Point source time estimation
    time=(instr.NEFD*args.filtering/transmission/args.rms)**2
    # Mapping source time estimation
    if not args.Point:
        time *= (1.+args.Xsize*60.*args.Ysize*60./instr.Np/instr.Sp**2)
    time *= args.overheads # adding overhead
    return timedelta(seconds=time)

def format_time(time):
    if time >= 3600.:
        return str(round(time/3600,1))+' hours'
    if time <= 3600. and time >= 60.:
        return str(round(time/60,1))+' minutes'
    if time <= 60. and time >= 0.1:
        return str(round(time,1))+' seconds'
    if time <= 0.1:
        return '<0.1 seconds'


if __name__ == "__main__":

    # Define the argument parser with default values

    parser = argparse.ArgumentParser(description="Time estimator for the IRAM30m continuum instrument v "+__version__,  \
                                     epilog='Based on the Billot et al. (2014) time estimator guide.')
    parser.add_argument('--camera',    help='IRAM 30m continuum camera.', choices=['GISMO','NIKA1mm', 'NIKA2mm'], required=True)
    parser.add_argument('--rms',       help='Any value above the confussion limit [mJy].', type=float, default=1)
    parser.add_argument('--pwv',       help='Precipitable water vapor [mm].', type=int, choices=[2,4,7], default=4)
    parser.add_argument('--elevation', help='elevation of the source, values from 20 to 83 degrees', type=float, default=45.)
    parser.add_argument('--Point',     help='Point source mode or...', action='store_true')
    parser.add_argument('--Xsize',     help='mapping size along the X direction [arcmin] and ... ', type=float, default=1.)
    parser.add_argument('--Ysize',     help='the Y direction, must be equal (point sources or larger than the Field of View [arcmin]', type=float, default=1.)
    parser.add_argument('--filtering', help='Data filtering scheme. Values from 1 to 4.', type=float, default=2.)
    parser.add_argument('--overheads', help='Telescope/observing overheads (calibration, pointing, focus). Values from 1.6 to 2.6', type=float, default=2)

    args = parser.parse_args()
    instr,tau = select_instrument(args.camera)
    time = time_estimator(args, instr)

    print ''
    print parser.description
    print '\n'
    print '        camera => ', args.camera
    print '           rms => ', args.rms, ' mJy/beam'
    print '           pwv => ', args.pwv, ' mm'
    print '     elevation => ', args.elevation, ' deg'
    if args.Point:
        print '   Xsize Ysize => Point source'
    else:
        print '   Xsize Ysize => ', args.Xsize, args.Ysize, ' arcmin'
    print '     overheads => ', args.overheads
    print '     filtering => ', args.filtering
    print ''
    print '    Total time => ', format_time(time.seconds)
    print '               => ', str(time)+ ' [days, hh:mm:ss]'
    print ''

    print 'Please include the following text into your proposal:\n'
    print 'The total observing time using '+ args.camera+' to map a region of '+str(args.Xsize)+'x'+str(args.Ysize)+ \
        ' arcminutes to reach an rms of '+str(args.rms)+' mJy/beam, assuming '+str(args.pwv)+' mm pwv, '+ \
        str(args.elevation)+' deg elevation, Ffilter='+str(args.filtering)+', Foverhead='+ \
        str(args.overheads)+ ', was estimated to be '+format_time(time.seconds)+', using the time estimator ' + __version__

    sys.exit()
