import numpy as np
from astropy import units as u
from astropy import constants as cst
from astropy.cosmology import Planck13 as cosmo

d_l = cosmo.luminosity_distance

Kkmspc2 = u.K * u.km / u.s * u.pc**2
Jykms   = u.Jy * u.km / u.s

nu_CO32 = 345.79*u.GHz
nu_CO10 = 115.271*u.GHz

nu_rest = nu_CO32
nu_obs = 99.105 * u.GHz
L_prime_CO = 3e9 * Kkmspc2
fwhm = 300 * u.km / u.s


# nu_rest = nu_CO10
# nu_obs = 111.932 * u.GHz
# L_prime_CO = 6.01 * Kkmspc2
# fwhm = 300 * u.km / u.s


z = nu_rest / nu_obs - 1


L_CO = L_prime_CO * 8 * np.pi * cst.k_B *nu_rest**3 / cst.c **3
L_CO = L_CO.to(u.Lsun)
I_CO = L_CO / (4*np.pi * d_l(z)**2 / cst.c * nu_rest / (1+z) )
I_CO = I_CO.to(Jykms)

sigma = fwhm / (2 * np.sqrt(2*np.log(2)) )
peak_CO = I_CO / (sigma * np.sqrt( 2 * np.pi ) )
peak_CO = peak_CO.to(u.mJy)

# base on https://github.com/astropy/astropy/blob/master/docs/units/equivalencies.rst
# is_observable.pro ....
