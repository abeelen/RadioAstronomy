
"""
Working IDL script

PRO sigma_vs_time, TotalObservingTime, PdBI_Sensitivity, sigma, BANDPASS=bandpass

; TotalObservingTime in second
; sigma in mJy

nTime = N_ELEMENTS(TotalObservingTime)
nBand = PdBI_Sensitivity.nBand

JpK = [PdBI_Sensitivity.Band1.JpK,$
       PdBI_Sensitivity.Band2.JpK,$
       PdBI_Sensitivity.Band3.JpK]

Tsys = [PdBI_Sensitivity.Band1.Tsys,$
        PdBI_Sensitivity.Band2.Tsys,$
        PdBI_Sensitivity.Band3.Tsys]

etha = [PdBI_Sensitivity.Band1.etha,$
        PdBI_Sensitivity.Band2.etha,$
        PdBI_Sensitivity.Band3.etha]

Na   = PdBI_Sensitivity.Na
Nc   = PdBI_Sensitivity.Nc
NPol = PdBI_Sensitivity.NPol

IF NOT KEYWORD_SET(bandpass) THEN BEGIN
   B    = PdBI_Sensitivity.B*1.d9
END ELSE BEGIN
   B    = bandpass*1d9
ENDELSE


Ton = TotalObservingTime/1.6

sigma = DBLARR(nBand,nTime)
FOR I=0,Ntime-1 DO BEGIN
   sigma[*,I] = Jpk*Tsys/(etha*sqrt(Na*(Na-1)*Nc*(Ton[I])*B*Npol))*1.d3 ; mJy*sqrt(Ton/hours)

ENDFOR

END

FUNCTION PdBI_sigma, TotalObservingTime, PdBI_Sensitivity, BANDPASS=bandpass

; TotalObservingTime in second
; sigma in mJy

nTime = N_ELEMENTS(TotalObservingTime)
nBand = PdBI_Sensitivity.nBand

JpK = [PdBI_Sensitivity.Band1.JpK,$
       PdBI_Sensitivity.Band2.JpK,$
       PdBI_Sensitivity.Band3.JpK]

Tsys = [PdBI_Sensitivity.Band1.Tsys,$
        PdBI_Sensitivity.Band2.Tsys,$
        PdBI_Sensitivity.Band3.Tsys]

etha = [PdBI_Sensitivity.Band1.etha,$
        PdBI_Sensitivity.Band2.etha,$
        PdBI_Sensitivity.Band3.etha]

Na   = PdBI_Sensitivity.Na
Nc   = PdBI_Sensitivity.Nc
NPol = PdBI_Sensitivity.NPol

IF NOT KEYWORD_SET(bandpass) THEN BEGIN
   B    = PdBI_Sensitivity.B*1.d9
END ELSE BEGIN
   B    = bandpass*1d9
ENDELSE

Ton = TotalObservingTime/1.6

sigma = DBLARR(nBand,nTime)
FOR I=0,Ntime-1 DO BEGIN
   sigma[*,I] = Jpk*Tsys/(etha*sqrt(Na*(Na-1)*Nc*(Ton[I])*B*Npol))*1.d3 ; mJy*sqrt(Ton/hours)

ENDFOR

RETURN, sigma

END

FUNCTION PdBI_time, sigma, PdBI_Sensitivity, BANDPASS=bandpass

; sigma in mJy

; TotalObservingTime in second

IF N_ElEMENTS(sigma) NE 3 THEN BEGIN
   MESSAGE, "sigma must have 3 elements"
ENDIF

nBand = PdBI_Sensitivity.nBand

JpK = [PdBI_Sensitivity.Band1.JpK,$
       PdBI_Sensitivity.Band2.JpK,$
       PdBI_Sensitivity.Band3.JpK]

Tsys = [PdBI_Sensitivity.Band1.Tsys,$
        PdBI_Sensitivity.Band2.Tsys,$
        PdBI_Sensitivity.Band3.Tsys]

etha = [PdBI_Sensitivity.Band1.etha,$
        PdBI_Sensitivity.Band2.etha,$
        PdBI_Sensitivity.Band3.etha]

Na   = PdBI_Sensitivity.Na
Nc   = PdBI_Sensitivity.Nc
NPol = PdBI_Sensitivity.NPol

IF NOT KEYWORD_SET(bandpass) THEN BEGIN
   B    = PdBI_Sensitivity.B*1.d9
END ELSE BEGIN
   B    = bandpass*1d9
ENDELSE



Ton = (Jpk*Tsys/((sigma/1.d3)*etha*sqrt(Na*(Na-1)*Nc*B*Npol)))^2 ; mJy*sqrt(Ton/hours)

TotalObservingTime = Ton * 1.6

RETURN, TotalObservingTime

END

;
; From http://www.iram.fr/GENERAL/calls/s08/s08/node33.html
;

void = {Band_Sensitivity, Wave: 0., JpK: 0., Tsys: 0., etha: 0.}

void = {PdBI_Sensitivy,$
        nBand: 3,$
        Band1: {Band_Sensitivity, 3.0, 0., 0., 0.},$
        Band2: {Band_Sensitivity, 2.0, 0., 0., 0.},$
        Band3: {Band_Sensitivity, 1.3, 0., 0., 0.},$
        Na: 6.,$        ;; Number of Antenna
        Nc: 1.,$        ;; Number of Configuration
        Npol: 2.,$      ;; Number of Polarization
        B: 2. $         ;; BandPass (GHz)
       }

Summer08 = {PdBI_Sensitivity,$
            nBand: 3 ,$
            Band1: {Band_Sensitivity, 3.0, 22., 100., 0.9},$
            Band2: {Band_Sensitivity, 2.0, 29., 180., 0.8},$
            Band3: {Band_Sensitivity, 1.3, 35., 250., 0.6},$
            Na: 5,$   ;; Number of Antenna
            Nc: 1,$   ;; Number of Configuration
            Npol: 2,$ ;; Number of Polarization
            B: 2. $    ;; BandPass (GHz)
           }

Winter07 = {PdBI_Sensitivity,$
            nBand: 3 ,$
            Band1: {Band_Sensitivity, 3.0, 22., 100., 0.9},$
            Band2: {Band_Sensitivity, 2.0, 29., 130., 0.85},$
            Band3: {Band_Sensitivity, 1.3, 35., 200., 0.8},$
            Na: 6,$   ;; Number of Antenna
            Nc: 1,$   ;; Number of Configuration
            Npol: 2,$ ;; Number of Polarization
            B: 2. $    ;; BandPass (GHz)
           }


Winter08 = {PdBI_Sensitivity,$
            nBand: 3 ,$
            Band1: {Band_Sensitivity, 3.0, 22., 100., 0.9},$
            Band2: {Band_Sensitivity, 2.0, 29., 150., 0.85},$
            Band3: {Band_Sensitivity, 1.3, 35., 200., 0.8},$
            Na: 6,$   ;; Number of Antenna
            Nc: 1,$   ;; Number of Configuration
            Npol: 2,$ ;; Number of Polarization
            B: 2. $    ;; BandPass (GHz)
           }

Summer09 = {PdBI_Sensitivity,$
            nBand: 3 ,$
            Band1: {Band_Sensitivity, 3.0, 22., 100., 0.9},$
            Band2: {Band_Sensitivity, 2.0, 29., 180., 0.8},$
            Band3: {Band_Sensitivity, 1.3, 35., 250., 0.6},$
            Na: 5,$   ;; Number of Antenna
            Nc: 1,$   ;; Number of Configuration
            Npol: 2,$ ;; Number of Polarization
            B: 2. $    ;; BandPass (GHz)
           }

Winter09 = {PdBI_Sensitivity,$
            nBand: 3 ,$
            Band1: {Band_Sensitivity, 3.0, 22., 100., 0.9},$
            Band2: {Band_Sensitivity, 2.0, 29., 150., 0.85},$
            Band3: {Band_Sensitivity, 1.3, 35., 200., 0.8},$
            Na: 6,$   ;; Number of Antenna
            Nc: 1,$   ;; Number of Configuration
            Npol: 2,$ ;; Number of Polarization
            B: 2. $    ;; BandPass (GHz)
           }
SummerWidex = {PdBI_Sensitivity,$
               nBand: 3 ,$
               Band1: {Band_Sensitivity, 3.0, 22., 100., 0.9},$
               Band2: {Band_Sensitivity, 2.0, 29., 180., 0.8},$
               Band3: {Band_Sensitivity, 1.3, 35., 250., 0.6},$
               Na: 5,$   ;; Number of Antenna
               Nc: 1,$   ;; Number of Configuration
               Npol: 2,$ ;; Number of Polarization
               B: 4. $    ;; BandPass (GHz)
              }

WinterWidex = {PdBI_Sensitivity,$
               nBand: 3 ,$
               Band1: {Band_Sensitivity, 3.0, 22., 100., 0.9},$
               Band2: {Band_Sensitivity, 2.0, 29., 150., 0.85},$
               Band3: {Band_Sensitivity, 1.3, 35., 200., 0.8},$
               Na: 6,$    ;; Number of Antenna
               Nc: 1,$    ;; Number of Configuration
               Npol: 2,$  ;; Number of Polarization
               B: 3.6 $     ;; BandPass (GHz)
               }

Summer09Widex = {PdBI_Sensitivity,$
               nBand: 3 ,$
               Band1: {Band_Sensitivity, 3.0, 22., 100., 0.9},$
               Band2: {Band_Sensitivity, 2.0, 29., 180., 0.8},$
               Band3: {Band_Sensitivity, 1.3, 35., 250., 0.6},$
               Na: 5,$   ;; Number of Antenna
               Nc: 1,$   ;; Number of Configuration
               Npol: 2,$ ;; Number of Polarization
               B: 3.6 $    ;; BandPass (GHz)
              }


Winter13Widex = {PdBINew_Sensitivity,$
                 nBand: 3 ,$ ;; bad bad bad
               Band1: {Band_Sensitivity, 3.0, 22., 100., 0.9},$
               Band2: {Band_Sensitivity, 2.0, 29., 170., 0.85},$
               Band3: {Band_Sensitivity, 1.3, 35., 200., 0.8},$
               Band4: {Band_Sensitivity, 0.8, 45., 370., 0.7},$
               Na: 6,$    ;; Number of Antenna
               Nc: 1,$    ;; Number of Configuration
               Npol: 2,$  ;; Number of Polarization
               B: 3.6 $     ;; BandPass (GHz)
               }

Winter14Widex = {PdBINew_Sensitivity,$
                 nBand: 3 ,$ ;; bad bad bad
                 Band1: {Band_Sensitivity, 3.0, 22., 130., 0.9},$
                 Band2: {Band_Sensitivity, 2.0, 29., 150., 0.85},$
                 Band3: {Band_Sensitivity, 1.3, 35., 200., 0.8},$
                 Band4: {Band_Sensitivity, 0.8, 45., 370., 0.7},$
                 Na: 6,$    ;; Number of Antenna
                 Nc: 1,$    ;; Number of Configuration
                 Npol: 2,$  ;; Number of Polarization
                 B: 3.6 $   ;; BandPass (GHz)
                }

Winter14Noema = {PdBINew_Sensitivity,$
                 nBand: 3 ,$ ;; bad bad bad
                 Band1: {Band_Sensitivity, 3.0, 22., 130., 0.9},$
                 Band2: {Band_Sensitivity, 2.0, 29., 150., 0.85},$
                 Band3: {Band_Sensitivity, 1.3, 35., 200., 0.8},$
                 Band4: {Band_Sensitivity, 0.8, 45., 370., 0.7},$
                 Na: 7,$    ;; Number of Antenna
                 Nc: 1,$    ;; Number of Configuration
                 Npol: 2,$  ;; Number of Polarization
                 B: 3.6 $   ;; BandPass (GHz)
                }



END

"""
